# [SpiderWire](https://ErmineII.github.io/spiderwire.html)

![SpiderWire preview](preview/SpiderWire.png)

- thin
- line spacing problems
- includes *some* greek (no diacritics), most of latin supplement, *some* miscellaneous technical

# [Befont](https://ErmineII.github.io/befont.html)

![Befont preview](preview/Befont.png)

- ASCII, some latin supplement punctuation
- constraints:
   - no curves
   - all points are on a 5x5 grid (ascenders & descenders have a height of 2 of these squares)

# [PoulTre](https://ErmineII.github.io/poultre.html)

![PoulTre preview](preview/PoulTre.png)

- ASCII
- constraints:
   - 1x5 or 3x5 or 5x5 grid with 1 unit for spacing and 2 units for ascenders & descenders
   - no diagonals
   - no fake diagonals:

```
this:     but not:
#   #     #     #
## ##      #   #\_ fake diagonal
 ###        # #
  #          #

```

## PoultreMono

![Poultre preview, monospace](preview/PoultreMono.png)

- all ^^ constraints + only 5x5 width.
- ASCII, [BQN](https://mlochbaum.github.io/BQN/) symbols are [being added](https://codeberg.org/Wezl/fonts/pulls/3), but there are rendering problems and the font editor [doesn't support](https://github.com/glyphr-studio/Glyphr-Studio-1/issues/274) symbols off the [BMP](https://en.wikipedia.org/wiki/Plane_(Unicode)#Basic_Multilingual_Plane "Basic Multilingual Plane").
- bitmap version in the FontX2 format made in [fedit95](https://github.com/neri/fedit95), but I don't know how to convert it to a usable format:

![bitmap fedit95 poultreMono](poultrem.png)

- you can see some [more](preview/poultreM_scheme_sample.png) [samples](preview/poultreM_APL_preview.png) in actual code.

---

These fonts are licensed under the ~~AGPLv3~~ I'm changing to the [OFL](https://en.wikipedia.org/wiki/SIL_Open_Font_License "SIL open font license"), because I don't know the rules for embedding with the AGPL.